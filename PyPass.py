import random
from time import sleep

print("What would you like to do?\n1. Generate password\n2. Quit")
action = input(">")

if action == "1":
    print("How many characters?")
    chars = input(">")

    Lwr = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
    Upr = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
    Smbl = ['"','\'','`','~','!','@','#','$','%','^','&','*','(',')','-','_','=','+','[','{','}',']','\\','|;',':',',','<','.','>','/','?']
    Nmbr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
    act = [Lwr, Upr, Smbl, Nmbr]

    RndChoice = random.choice(act)
    RndChar = random.choice(RndChoice)

    print(RndChoice)
    print(RndChar)
elif action == "2":
    print("Goodbye!")
    sleep(2)
    quit()